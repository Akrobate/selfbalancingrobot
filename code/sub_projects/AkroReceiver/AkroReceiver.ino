/**
 *  Transmitter example implementation
 *  
 *  HardWare: Teensy 2++
 * 
 *  Code used to be Host in communication initialization
 * 
 */

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <arduino.h>

#include "protocol/AkroRcProtocol.h"

#define CE_PIN 44 // @todo adapt
#define CSN_PIN 45 // @todo adapt

void setup() {
    Serial.begin(9600);
    Serial.println("starting....");

    
    // Initing protocol
    RF24 * radio = new RF24(CE_PIN, CSN_PIN);
    akro_rc_protocol = new AkroRcProtocol();
    akro_rc_protocol->injectRF24(radio);

    // Forcing channel to be setted (bypassing meeting)
    akro_rc_protocol->setChannel(90);
    akro_rc_protocol->setEmissionAddress("PIPE2");
    akro_rc_protocol->setReceiveAddress("PIPE1");
    akro_rc_protocol->setSignalPower(RF24_PA_HIGH);
    akro_rc_protocol->setDataRate(RF24_250KBPS);

    akro_rc_protocol->openPipes();
    akro_rc_protocol->init();
}


void loop() {

    // Check data available
    if (akro_rc_protocol->isAvailableReadData()) {
        // Debugging code
        Serial.println("Data available for read");
        akro_rc_protocol->read();
    }

    delay(100);
}
