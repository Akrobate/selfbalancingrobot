#include <Arduino.h>
#include <AccelStepper.h>

#define STEP_PIN 14
#define DIR_PIN 16

AccelStepper stepper(AccelStepper::DRIVER, STEP_PIN, DIR_PIN);

int speed = 200*16 * 4;

void setup() {
    Serial.begin(115200);
    delay(100);
    stepper.setMaxSpeed(speed);
    stepper.setSpeed(speed);
}

long _time = 0;

void loop() {
    if (millis() - _time > 2000) {
        _time = millis();
        speed = -speed;
        stepper.setSpeed(speed);
    }
    stepper.runSpeed();
}
