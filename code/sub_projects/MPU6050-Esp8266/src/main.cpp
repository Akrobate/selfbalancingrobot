#include <Arduino.h>
#include <Wire.h>
#include <MPU6050.h>


MPU6050 mpu(Wire); // Initialisation du capteur
unsigned long timer = 0;

void setup() {
    Serial.begin(115200);
    delay(100);

    pinMode(D1, OUTPUT);  // SCL
    pinMode(D2, OUTPUT);  // SDA
    digitalWrite(D1, LOW);
    digitalWrite(D2, LOW);
    delay(100);
    pinMode(D1, INPUT_PULLUP);  // Remettre en mode I2C
    pinMode(D2, INPUT_PULLUP);

    delay(100);
    Wire.begin(); 

    delay(1000);
    Wire.beginTransmission(MPU6050_ADDR);
    Wire.write(0x6B);  // PWR_MGMT_1 register
    //Wire.write(0x80);  // Reset command
    Wire.write(0x00);
    Wire.endTransmission();
    delay(100);

    delay(1000);
    byte status = mpu.begin();
    
    Serial.print("MPU6050 status: ");
    Serial.println(status);
    
    if (status != 0) {
        Serial.println("Erreur de communication avec le MPU6050 !");
        while (1);
    }

    Serial.println("Calibrage du capteur...");
    mpu.calcOffsets(true, true);  // Calibrage des offsets (gyroscope et accéléromètre)
    Serial.println("Calibrage terminé !");

}

int i = 0;

void loop() {
    mpu.update(); // Met à jour les mesures avec le filtrage complémentaire intégré

    // Lecture des angles filtrés
    float roll = mpu.getAngleX();
    float pitch = mpu.getAngleY();
    float yaw = mpu.getAngleZ();
    
    float ax = mpu.getAccX();
    float ay = mpu.getAccY();
    float az = mpu.getAccZ();


    i++;
    if (millis() - timer > 1000) {  // Affichage toutes les 100 ms
        Serial.print(i);
        i = 0;
        Serial.print(" Roll: ");
        Serial.print(roll);
        Serial.print("° | Pitch: ");
        Serial.print(pitch);
        Serial.print("° | Yaw: ");
        Serial.print(yaw);
        Serial.print("°");
        
        Serial.print(" ax: ");
        Serial.print(ax);
        Serial.print(" ay: ");
        Serial.print(ay);
        Serial.print(" az: ");
        Serial.print(az);

        Serial.print(" rawAccX: ");
        Serial.print(mpu.rawAccX);
        Serial.print(" rawAccY: ");
        Serial.print(mpu.rawAccY);
        Serial.print(" rawAccZ: ");
        Serial.print(mpu.rawAccZ);


        Serial.println("");



        timer = millis();


    }
}
