#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include "printf.h"

#define CE_PIN 44
#define CSN_PIN 45

RF24 radio(CE_PIN,CSN_PIN);


//
// Channel info
//

const uint8_t num_channels = 128;
uint8_t values[num_channels];

void setup(void) {

    Serial.begin(115200);
    printf_begin();

    radio.begin();
    radio.setAutoAck(false);

    radio.startListening();
    radio.stopListening();

    int i = 0;
    while (i < num_channels) {
        printf("%x",i>>4);
        i++;
    }
    printf("\n\r");
    i = 0;
    while (i < num_channels) {
        printf("%x",i&0xf);
        i++;
    }
    printf("\n\r");
}


const int num_reps = 100;

void loop(void) {

    memset(values, 0, sizeof(values));

    // Scan all channels num_reps times
    int rep_counter = num_reps;
    while (rep_counter--) {
        int i = num_channels;
        while (i--) {
            radio.setChannel(i);

            radio.startListening();
            delayMicroseconds(225);

            if ( radio.testCarrier() ){
                values[i]++;
            }
            radio.stopListening();
        }
    }

    // Print out channel measurements, clamped to a single hex digit
    int i = 0;
    while (i < num_channels) {
        printf("%x",min(0xf,values[i]&0xf));
        i++;
    }
    printf("\n\r");
}
