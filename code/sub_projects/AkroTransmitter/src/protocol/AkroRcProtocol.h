#ifndef AkroRcProtocol_h
#define AkroRcProtocol_h

/**
 * https://passionelectronique.fr/tutorial-nrf24l01/
 * 
 */

#include <SPI.h>
#include <RF24.h>


#define ARP_DEFAULT_MEETING_CHANNEL 83
#define ARP_CHANNEL_COUNT 125
#define ARP_ADDRESS_SIZE_COUNT 5

struct PairingData {
    int channel;
    byte emission_address[ARP_ADDRESS_SIZE_COUNT];
    byte receive_address[ARP_ADDRESS_SIZE_COUNT];
};

class AkroRcProtocol {

    public:

        AkroRcProtocol();

        void scanChannels();
        void sendPairingData();
        void listenPairingData();
        
        void init();
        void send(void * message, int size);
        void read(void * message, int size);

        void preparePairingData();

        bool isAvailableReadData();

        int getChannel();
        void setChannel(int channel);

        bool isMeetingMaster();
        void setIsMeetingMaster(bool is_meeting_master);

        void injectRF24(RF24 * radio);

        void setEmissionAddress(byte * address);
        byte * getEmissionAddress();

        void setReceiveAddress(byte * address);
        byte * getReceiveAddress();

        void setSignalPower(rf24_pa_dbm_e power);
        rf24_pa_dbm_e getSignalPower();

        void setDataRate(rf24_datarate_e data_rate);
        rf24_datarate_e getDataRate();
 
        void openPipes();

        void startListeningIfNotListening();


    private:

        PairingData _pairing_data;

        RF24 * radio = NULL;

        bool is_meeting_master;

        bool _is_listening = false;

        int meeting_channel = ARP_DEFAULT_MEETING_CHANNEL;
        byte meeting_address[ARP_ADDRESS_SIZE_COUNT];

        int channel = 0;

        rf24_pa_dbm_e signal_power;
        rf24_datarate_e data_rate;

        byte emission_address[ARP_ADDRESS_SIZE_COUNT];
        byte receive_address[ARP_ADDRESS_SIZE_COUNT];

        int channel_strenght_list[ARP_CHANNEL_COUNT];
        byte * generateRandomAddress();

        bool hasCarrier(int channel, int delay_micro);

};

#endif