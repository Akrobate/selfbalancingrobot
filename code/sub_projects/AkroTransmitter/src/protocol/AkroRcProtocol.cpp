#include "AkroRcProtocol.h"

AkroRcProtocol::AkroRcProtocol() {

}

void AkroRcProtocol::setIsMeetingMaster(bool is_meeting_master) {
    this->is_meeting_master = is_meeting_master;
}

bool AkroRcProtocol::isMeetingMaster() {
    return this->is_meeting_master;
}

void AkroRcProtocol::injectRF24(RF24 * radio) {
    this->radio = radio;
}

void AkroRcProtocol::scanChannels() {

}

void AkroRcProtocol::sendPairingData() {
    this->send(&this->_pairing_data, sizeof(PairingData));
}

void AkroRcProtocol::listenPairingData() {
    if (this->isAvailableReadData()) {
        this->read(&this->_pairing_data, sizeof(PairingData));
    }
}

void AkroRcProtocol::send(void * message, int size) {
    radio->stopListening();
    radio->write(message, size);
    radio->startListening();
}

// @todo message should probably be property of instance
void AkroRcProtocol::read(void * message, int size) {
    this->startListeningIfNotListening();
    this->radio->read(message, size);
}


bool AkroRcProtocol::isAvailableReadData() {
    this->startListeningIfNotListening();
    return this->radio->available();
}


void AkroRcProtocol::startListeningIfNotListening() {
    if (!this->_is_listening) {
        radio->startListening();
        this->_is_listening = true;
    }
}

byte * AkroRcProtocol::generateRandomAddress() {
    byte generated_random_address[ARP_ADDRESS_SIZE_COUNT];
    for (int i = 0; i < ARP_ADDRESS_SIZE_COUNT; i++) {
        generated_random_address[i] = random(0, 255);
    }
    return generated_random_address;
}


bool AkroRcProtocol::hasCarrier(int channel, int delay_micro) {
    bool has_carrier = false;
    this->radio->setAutoAck(false);
    this->radio->setChannel(channel);
    this->radio->startListening();
    delayMicroseconds(delay_micro);
    has_carrier = this->radio->testCarrier();
    this->radio->stopListening();
    return has_carrier;
}


void AkroRcProtocol::init() {
    this->radio->begin();
    this->radio->startListening();
    this->radio->stopListening();
    this->_is_listening = false;
}


int AkroRcProtocol::getChannel() {
    return this->channel;
}

void AkroRcProtocol::setChannel(int channel) {
    this->channel = channel;
}


void AkroRcProtocol::setReceiveAddress(byte * address) {
    for (int i = 0; i < ARP_ADDRESS_SIZE_COUNT; i++) {
        this->receive_address[i] = address[i];
    }
}


byte * AkroRcProtocol::getReceiveAddress() {
    return this->receive_address;
}


void AkroRcProtocol::setEmissionAddress(byte * address) {
    for (int i = 0; i < ARP_ADDRESS_SIZE_COUNT; i++) {
        this->emission_address[i] = address[i];
    }
}

byte * AkroRcProtocol::getEmissionAddress() {
    return this->emission_address;
}

/**
 * RF24_PA_MIN : pour un niveau d’émission minimal (idéal si vous n’avez pas besoin de communiquer à longue distance, ou si vous alimentez votre module nRF24L01 depuis l’alimentation 3,3 V de votre arduino)
 * RF24_PA_LOW : niveau d’émission bas
 * RF24_PA_HIGH : niveau d’émission moyen/haut
 * RF24_PA_MAX : pour émettre à grande distance (jusqu’à 1,1 km de distance, dans des conditions optimales)
 */
void AkroRcProtocol::setSignalPower(rf24_pa_dbm_e power) {
    this->radio->setPALevel(power);
    this->signal_power = power;
}

rf24_pa_dbm_e AkroRcProtocol::getSignalPower() {
    return this->signal_power;
}

/**
 * RF24_250KBPS, RF24_1MBPS, ou encore, RF24_2MBPS
 */
void AkroRcProtocol::setDataRate(rf24_datarate_e data_rate) {
    this->radio->setDataRate(data_rate);
    this->data_rate = data_rate;
}

rf24_datarate_e AkroRcProtocol::getDataRate() {
    return this->data_rate;
}

void AkroRcProtocol::openPipes() {
    this->radio->openWritingPipe(this->emission_address);
    this->radio->openReadingPipe(1, this->receive_address);
}

// @todo implement: Will set the Pariring data object
void AkroRcProtocol::preparePairingData() {
    // this->_pairing_data->channel
    // this->_pairing_data->emission_address
    // this->_pairing_data->receive_address
}