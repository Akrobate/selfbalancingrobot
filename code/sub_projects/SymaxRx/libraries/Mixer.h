#ifndef Mixer_h
#define Mixer_h

/**
 * Delta Mixer
 * 
 * Controls X / Y are signed integer values from -127 to 127
 * Output Left / Right are signed values from -127 to 127
 * 
 */

class Mixer
{
    public:
        Mixer(int mode = 1, bool is_left_inverted = false, bool is_right_inverted = false);
        void process();
        void setXControl(int x_position);
        void setYControl(int y_position);

        void setInvertLeftOutput(bool is_left_inverted);
        void setInvertRightOutput(bool is_right_inverted);

        int getLeftOutput();
        int getRightOutput();

    private:
        int is_left_inverted;
        bool left_inverter;

        int is_right_inverted;
        bool right_inverter;

        int x_position;
        int y_position;

        int left_output;
        int right_output;

        int minMaxCapping(int value);


};

#endif
