#include "Mixer.h"

Mixer::Mixer(
    int mode = 1, // @todo put constant instead
    bool is_left_inverted = false,
    bool is_right_inverted = false
) {
    this->setInvertLeftOutput(is_left_inverted);
    this->setInvertRightOutput(is_right_inverted);
}


void Mixer::process() {
    // Mixing function
    if (this->mode == 1) {
        this->additionAndCapping();
    } else {
        this->meanMixing();
    }

    this->applyInverter();
}


/**
 *  Mode 1
 */
void Mixer::additionAndCapping() {
    this->left_output = this->y_position + this->x_position;
    this->right_output = this->y_position + (this->x_position * -1);

    this->left_output = this->minMaxCapping(this->left_output);
    this->right_output = this->minMaxCapping(this->right_output);
}

/**
 *
 */
int Mixer::minMaxCapping(int value) {
    int out_value = value;
    if (value > 127) {
        out_value = 127;
    }

    if (value < -127) {
        out_value = -127;
    }

    return out_value;
}

/**
 *  Mode 2
 */
void Mixer::meanMixing() {

    int remapped_y_position = map(this->y_position, -127, 127, -63, 63);
    int remapped_x_position = map(this->x_position, -127, 127, -63, 63);

    this->left_output = remapped_y_position + remapped_x_position;
    this->right_output = remapped_y_position + (remapped_x_position * -1);
}


void Mixer::applyInverter() {
    this->left_output = this->left_output * this->left_inverter;
    this->right_output = this->left_output * this->left_inverter;
}


void Mixer::setInvertLeftOutput(bool is_left_inverted) {
    this->is_left_inverted = is_left_inverted;
    this->left_inverter = 1;
    if (this->is_left_inverted) {
        this->left_inverter = -1;
    }
}


void Mixer::setInvertRightOutput(bool is_right_inverted) {
    this->is_right_inverted = is_left_inverted;
    this->right_inverter = 1;
    if (this->is_right_inverted) {
        this->right_inverter = -1;
    }
}


void Mixer::setXControl(int x_position) {
    this->x_position = x_position;
}


void Mixer::setYControl(int y_position) {
    this->y_position = y_position;
}


int Mixer::getLeftOutput() {
    return this->left_output;
}


int Mixer::getRightOutput() {
    return this->right_output;
}

