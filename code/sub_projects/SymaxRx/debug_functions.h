
/**
 * DebugDisplay
 * @return {void} 
 */
void DebugDisplay(rx_values_t rx) {
    Serial.print(" :\t thr ");
    Serial.print(rx.throttle);
    
    Serial.print("\t y ");
    Serial.print(rx.yaw);
    
    Serial.print("\t p");
    Serial.print(rx.pitch);
    
    Serial.print("\t r ");
    Serial.print(rx.roll);
    
    Serial.print("\t t_y ");
    Serial.print(rx.trim_yaw);

    Serial.print("\t t_p ");
    Serial.print(rx.trim_pitch);
    
    Serial.print("\t t_r ");
    Serial.print(rx.trim_roll);
    
    Serial.print("\t vid ");
    Serial.print(rx.video);
    
    Serial.print("\t pic ");
    Serial.print(rx.picture);
    
    Serial.print("\t hSpeed ");
    Serial.print(rx.highspeed);
    
    Serial.print("\t flip ");
    Serial.println(rx.flip);
    
    Serial.print("\n");
}
