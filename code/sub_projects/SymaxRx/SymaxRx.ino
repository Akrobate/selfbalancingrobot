/**
 * Generic receiver for Syma
 * Adapted to work with
 *  - AtMega 328P : set HARDWARE_ATMEGA328P 1
 *  - Teensy 2 ++ : set HARDWARE_TEENSY_2 1
 * 
 * To log outputs on serial port:
 *  set DEBUG 1
 * 
 * In production DEBUG must be disable to avoid loop lag
 * 
 * PINS configurations are done in configuration.h
 * 
 * @todo:
 * - Check trims configuration
 * - add new vars for trim configuration holder
 * - update real time position from trims offests 
 */ 

#include <SPI.h>
#include <symax_protocol.h>
#include <Servo.h>

// Configurations
#define HARDWARE_TEENSY_2 1
#include "configuration.h"
#include "debug_function.h"

#define DEBUG 1

void updateTrimPositions();
void updateServoPositions();
void updateOnOff();
void configureServoPinout();
void configureOnOffPinout();
void configureWirelessPinout();

nrf24l01p wireless;
symaxProtocol protocol;

Servo * servo_1 = new Servo();
Servo * servo_2 = new Servo();
Servo * servo_3 = new Servo();
Servo * servo_4 = new Servo();

int trim_yaw = 0;
int trim_pitch = 0;
int trim_roll = 0; 

rx_values_t rxValues;

void setup() {

    if (DEBUG) {
        Serial.print("setup started...");
        Serial.begin(115200);
    }

    configureServoPinout();
    configureOnOffPinout();
    configureWirelessPinout();

    if (DEBUG) {
        Serial.print("setup done");
    }
}


void loop() {
    uint8_t value = protocol.run(&rxValues); 
    if (value == BOUND_NEW_VALUES) {

        updateTrimPositions();
        updateServoPositions();
        updateOnOff();

        if (DEBUG) {
            DebugDisplay(rxValues);
        }
    }
}


void updateServoPositions() {
    servo_1->write(map(rxValues.yaw, -127, 127, SERVO_1_MIN_ANGLE, SERVO_1_MAX_ANGLE));
    servo_2->write(map(rxValues.pitch, -127, 127, SERVO_2_MIN_ANGLE, SERVO_2_MAX_ANGLE));
    servo_3->write(map(rxValues.roll, -127, 127, SERVO_3_MIN_ANGLE, SERVO_3_MAX_ANGLE));
    servo_4->write(map(rxValues.throttle, -127, 127, SERVO_4_MIN_ANGLE, SERVO_4_MAX_ANGLE));
}


void updateTrimPositions() {
    trim_yaw = rx.trim_yaw;
    trim_pitch = rx.trim_pitch;
    trim_roll = rx.trim_roll;
}


void updateOnOff() {
    digitalWrite(ON_OFF_OUTPUT_PIN_1, rxValues.video)
    digitalWrite(ON_OFF_OUTPUT_PIN_2, rxValues.picture)
    digitalWrite(ON_OFF_OUTPUT_PIN_3, rxValues.highspeed)
    digitalWrite(ON_OFF_OUTPUT_PIN_4, rxValues.flip)
}


void configureServoPinout() {
    servo_1->attach(SERVO_1_PIN);
    servo_2->attach(SERVO_2_PIN);
    servo_3->attach(SERVO_3_PIN);
    servo_4->attach(SERVO_4_PIN);
}


void configureOnOffPinout() {
    pinMode(ON_OFF_OUTPUT_PIN_1, OUTPUT);
    pinMode(ON_OFF_OUTPUT_PIN_2, OUTPUT);
    pinMode(ON_OFF_OUTPUT_PIN_3, OUTPUT);
    pinMode(ON_OFF_OUTPUT_PIN_4, OUTPUT);
}

void configureWirelessPinout() {
    pinMode(SS, OUTPUT);
    wireless.setPins(WIRELESS_CE_PIN, WIRELESS_CS_PIN);
    // Set power (PWRLOW,PWRMEDIUM,PWRHIGH,PWRMAX)
    wireless.setPwr(PWRLOW);
    protocol.init(&wireless);
}