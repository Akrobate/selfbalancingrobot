# Symax controller

[library](https://github.com/Suxsem/symaxrx)
[Remote controller hack](https://www.firstquadcopter.com/news/syma-x5c-range-hack)


# Hardware

* Teensy 2 ++
* AtMega328P

# Configuration Hardware

## Chip selection

First in main code source file define what hardware you use with:

```C++
#define HARDWARE_TEENSY_2 1
```

Or

```C++
#define HARDWARE_ATMEGA328P 1
```

## Pin configuration


Second step define your hardware pinout in confuguration.h:

```C++
#define SERVO_1_PIN 8
#define SERVO_2_PIN 9
#define SERVO_3_PIN 10
#define SERVO_4_PIN 11

#define ON_OFF_OUTPUT_PIN_1 12
#define ON_OFF_OUTPUT_PIN_2 13
#define ON_OFF_OUTPUT_PIN_3 14
#define ON_OFF_OUTPUT_PIN_4 15

#define WIRELESS_CE_PIN 22
#define WIRELESS_CS_PIN 23
```

## Servo angles configurations

```C++
// Global Min / Max definitions
#define SERVO_MIN_ANGLE 0
#define SERVO_MAX_ANGLE 180

// Specific Min angle
#define SERVO_1_MIN_ANGLE SERVO_MIN_ANGLE
#define SERVO_2_MIN_ANGLE SERVO_MIN_ANGLE
#define SERVO_3_MIN_ANGLE SERVO_MIN_ANGLE
#define SERVO_4_MIN_ANGLE SERVO_MIN_ANGLE

// Specific Max angle
#define SERVO_1_MAX_ANGLE SERVO_MAX_ANGLE
#define SERVO_2_MAX_ANGLE SERVO_MAX_ANGLE
#define SERVO_3_MAX_ANGLE SERVO_MAX_ANGLE
#define SERVO_4_MAX_ANGLE SERVO_MAX_ANGLE
```