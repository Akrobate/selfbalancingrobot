#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Structure pour stocker les paramètres du PID
typedef struct {
    double Kp;
    double Ki;
    double Kd;
    double previous_error;
    double integral;
} PIDController;

// Fonction d'initialisation du PID
void initPID(PIDController *pid, double Kp, double Ki, double Kd) {
    pid->Kp = Kp;
    pid->Ki = Ki;
    pid->Kd = Kd;
    pid->previous_error = 0.0;
    pid->integral = 0.0;
}

// Fonction de calcul du PID
double computePID(PIDController *pid, double setpoint, double measured_value, double dt) {
    double error = setpoint - measured_value;
    pid->integral += error * dt;
    double derivative = (error - pid->previous_error) / dt;
    double output = (pid->Kp * error) + (pid->Ki * pid->integral) + (pid->Kd * derivative);
    pid->previous_error = error;
    return output;
}

// Exemple de système simulé
double simulate_system(double input) {
    static double system_state = 0.0;
    system_state += input * 0.1;  // Une simple intégration
    return system_state;
}

int main() {
    PIDController pid;
    initPID(&pid, 1.0, 0.1, 0.05);  // Réglages PID (à ajuster)

    double setpoint = 10.0;  // Valeur cible
    double measured_value = 0.0;
    double dt = 0.1;  // Pas de temps en secondes

    for (int i = 0; i < 100; i++) {
        double output = computePID(&pid, setpoint, measured_value, dt);
        measured_value = simulate_system(output);

        printf("Time: %.1f s, Output: %.2f, Measured: %.2f\n", i * dt, output, measured_value);
    }

    return 0;
}
