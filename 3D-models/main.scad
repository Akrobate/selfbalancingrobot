use <assets/Nema17Stepper.scad>

use <pieces/Nema17HolderV1Piece.scad>
use <pieces/BottomMiddleSeparatorPiece.scad>
use <pieces/BottomFacadePiece.scad>


Nema17HolderV1Piece_z_size = 6;
NEMA17_z_size = 34;

size_z = 100;




$fs=0.1;
$fa=0.1;

translate([0, -size_z / 2, 0])
rotate([90, 0, 0]) {
    Nema17HolderV1Piece();
    translate([0, 0, -Nema17HolderV1Piece_z_size / 2 - NEMA17_z_size])
        NEMA17(NEMA17_z_size);
}

rotate([90, 0, 0])
    BottomMiddleSeparatorPiece();


translate([0, size_z / 2, 0])
rotate([-90, 0, 0]) {
    Nema17HolderV1Piece();
    translate([0, 0, -Nema17HolderV1Piece_z_size / 2 - NEMA17_z_size])
        NEMA17(NEMA17_z_size);
}

translate([51 / 2 + 1.5, 0, 0])
    rotate([-90, 0, -90])
        BottomFacadePiece();

translate([-51 / 2 - 1.5, 0, 0])
    rotate([-90, 0, -90])
        BottomFacadePiece();


