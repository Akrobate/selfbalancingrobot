



module Nema17HolderV1Piece(
    x_size = 51,
    y_size = 42.3,
    z_size = 6
) {
    
    nema_fix_distance = 31;
    _fn = 30;
    insert_throw_diameter = 4.2;
    insert_throw_length = 6;
    throw_diameter = insert_throw_diameter;
    throw_depth = insert_throw_length;

    lateral_fixation_distance = 31;

    difference() {
        cube([x_size, y_size, z_size], center=true);

        for(i = [[-1, -1], [1, -1], [1, 1], [-1, 1]]) {
        color("red")
        translate([x_size / 2 * i[0], - lateral_fixation_distance / 2 * i[1], 0])
            rotate([0, 90, 0])
                cylinder(d = insert_throw_diameter, h = insert_throw_length * 2, center = true, $fn = _fn);
        }

        for(i = [[-1, -1], [1, -1], [1, 1], [-1, 1]]) {
            translate([nema_fix_distance / 2 * i[0], nema_fix_distance / 2 * i[1], 0]){
                cylinder(d = 3, h = 150, center = true, $fn = _fn);

                translate([0, 0, 3])
                    cylinder(d = 8, h = 6, center = true, $fn = _fn);
            }
        }

        translate([0, 0, 0])
            cylinder(d = 25, h = 150, center = true, $fn = _fn);

        translate([0, 0, 2])
            cylinder(d1 = 25, d2 = 35,  h = 5, center = true, $fn = _fn);
    }

}

Nema17HolderV1Piece();