



module BottomMiddleSeparatorPiece(
    x_size = 51,
    y_size = 42.3,
    z_size = 6
) {
    
    nema_fix_distance = 31;
    _fn = 30;
    insert_throw_diameter = 4.2;
    insert_throw_length = 6;
    throw_diameter = insert_throw_diameter;
    throw_depth = insert_throw_length;

    lateral_fixation_distance = 31;

    difference() {
        cube([x_size, y_size, z_size], center=true);

        for(i = [[-1, -1], [1, -1], [1, 1], [-1, 1]]) {
        color("red")
        translate([x_size / 2 * i[0], - lateral_fixation_distance / 2 * i[1], 0])
            rotate([0, 90, 0])
                cylinder(d = insert_throw_diameter, h = insert_throw_length * 2, center = true, $fn = _fn);
        }

        translate([0, 0, 0])
            cylinder(d = 35, h = 150, center = true, $fn = _fn);

    }

}

BottomMiddleSeparatorPiece();