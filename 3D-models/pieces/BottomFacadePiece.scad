

module barSubPiece(point_1_coords, point_2_coords, bars_width_size, z_size) {
    _fn = 30;
    hull() {
        translate(point_1_coords)
            cylinder(d = bars_width_size, h = z_size, center = true, $fn = _fn);

        translate(point_2_coords)
            cylinder(d = bars_width_size, h = z_size, center = true, $fn = _fn);
    }
} 



module BottomFacadePiece() {

    _fn = 30;

    throws_distance = 31;

    x_size = 100;
    y_size = 42.3;
    z_size = 3;

    vertical_bars_s_size = 6;
    bars_width_size = 5;

    throw_diameter_m3 = 3.5;

    difference() {

        union() {
            translate([-x_size / 2, 0, 0])
                cube([vertical_bars_s_size, y_size, z_size], center=true);

            cube([vertical_bars_s_size, y_size, z_size], center=true);

            translate([x_size / 2, 0, 0])
                cube([vertical_bars_s_size, y_size, z_size], center=true);



            point_top_left = [-x_size / 2, y_size / 2 - bars_width_size / 2, 0];
            point_bottom_left = [-x_size / 2, -y_size / 2 + bars_width_size / 2, 0];
            point_top_center = [0, y_size / 2 - bars_width_size / 2, 0];
            point_bottom_center = [0, -y_size / 2 + bars_width_size / 2, 0];
            
            point_top_right = [x_size / 2, y_size / 2 - bars_width_size / 2, 0];
            point_bottom_right = [x_size / 2, -y_size / 2 + bars_width_size / 2, 0];



            barSubPiece(point_top_left, point_top_center, bars_width_size, z_size);
            barSubPiece(point_bottom_left, point_bottom_center, bars_width_size, z_size);

            barSubPiece(point_bottom_left, point_top_center, bars_width_size, z_size);
            barSubPiece(point_top_left, point_bottom_center, bars_width_size, z_size);

            barSubPiece(point_top_right, point_top_center, bars_width_size, z_size);
            barSubPiece(point_bottom_right, point_bottom_center, bars_width_size, z_size);

            barSubPiece(point_bottom_right, point_top_center, bars_width_size, z_size);
            barSubPiece(point_top_right, point_bottom_center, bars_width_size, z_size);
        }


        translate([-x_size / 2, -throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

        translate([-x_size / 2, throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

        translate([0, -throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

        translate([0, throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

        translate([x_size / 2, -throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

        translate([x_size / 2, throws_distance / 2, 0])
            cylinder(d = throw_diameter_m3, h = z_size * 2, center=true, $fn = _fn);

    }

    //cube([vertical_bars_s_size, y_size, z_size], center=true);
//
    //translate([x_size / 2, 0, 0])
    //    cube([vertical_bars_s_size, y_size, z_size], center=true);




}


BottomFacadePiece();