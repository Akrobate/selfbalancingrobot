
stepperDemo();

module stepperDemo() {
  $fs=0.1;
  $fa=0.1;
  NEMA17(34);
}


module NEMA17(length) {
  stepperMotorChamfered(
    length=length, caseWidth=42, caseFlatsWidth=53.57, caseInnerFlatsWidth=50.25,
    boltPatternD=43.84, boltD=3, boltDepth=4.5,
    topThickness=10, topcutThickness=7.5, topProtrude=2, topProtrudeD=22, 
    topRecessOD=32.5, topRecessID=30.85, topRecessDepth=0.6,
    bottomThickness=11, bottomcutThickness=9,
    shaftD=5, shaftLength=24.5, flatD=4.5, flatLength=21, rearShaftLength=-3, shaftChamfer=0.5
  );
}

module NEMA17_2(length) {
  stepperMotorRounded(
    length=length, caseWidth=42, caseRadius=25,
    boltPatternD=42.43, boltD=3, boltDepth=4.5,
    topThickness=8, topProtrude=2, topProtrudeD=22, topHoleD=7.5,
    bottomThickness=9, bottomHoleD=7,
    topRecessOD=32.5, topRecessID=30.85, topRecessDepth=0.6,
    shaftD=5, shaftLength=24.5, flatD=4.5, flatLength=21, rearShaftLength=-3, shaftChamfer=0.5
  );
}


// Stepper motor with chamfered corners on caps. Caps extend past rest of body
module stepperMotorChamfered(
    length, caseWidth, caseFlatsWidth, caseInnerFlatsWidth,
    boltPatternD, boltD, boltDepth,
    topThickness, topcutThickness, topProtrude, topProtrudeD, 
    topRecessOD, topRecessID, topRecessDepth,
    bottomThickness, bottomcutThickness,
    shaftD, shaftLength, flatD, flatLength, rearShaftLength, shaftChamfer
  ) {

  color1 = [0.05,0.05,0.05];
  color2 = [0.15,0.15,0.15];
  alColor = [0.9,0.9,0.9];
  steelColor = [0.75,0.75,0.75];
  
  difference() {
    // end caps
     color(color2) linear_extrude(length+0.1) intersection() {
      square(caseWidth, center=true);
      rotate([0,0,45]) square(caseFlatsWidth, center=true);
    }
       
    // top face
    color(color2) translate([0,0,length-topRecessDepth]) difference() {
      cylinder(d=topRecessOD, h=topProtrude+1);
      rotate([180,0]) cylindricalChamferOD(topRecessOD, topRecessDepth);
      translate([0,0,-0.5]) cylinder(d=topProtrudeD-0.2,h=topProtrude*2);
    }
    
    // cut bare top face
    color(alColor) translate([0,0,length-0.02])
      linear_extrude(topProtrude+0.1, convexity=3) difference() {
        square(caseWidth+1,center=true);
        circle(d=(topProtrudeD+topRecessOD)/2, $fn=12);
      }
    
    // top bolt holes  
    color(alColor) translate([0,0,length-topThickness+1]) for (i=[0:3]) rotate(45+i*90) translate([boltPatternD/2,0]) cylinder(d=boltD, h=topThickness);

      
    // center cutout
    color(alColor) {
      translate([-caseWidth/2-1,-caseWidth/2-1,bottomThickness]) 
        cube([caseWidth+2,caseWidth+2,length-topThickness-bottomThickness]);
      translate([0,0,bottomcutThickness]) cylinder(d=caseInnerFlatsWidth+0.25,h=length-bottomcutThickness-topcutThickness);
    }
  }
  
  // top face protrusion
  difference() {
    color(alColor) translate([0,0,length-topProtrude]) cylinder(d=topProtrudeD+0.1, h=topProtrude*2);
    color(color2) translate([0,0,length+topProtrude]) cube([topProtrudeD+1,topProtrudeD+1,1], center=true);
  }

  // center section
  color(color1) translate([0,0,bottomcutThickness]) 
    linear_extrude(length-topcutThickness-bottomcutThickness) intersection() {
      square(caseWidth,center=true);
      circle(d=caseInnerFlatsWidth);
    }
    
  // motor shaft
  color(steelColor)
  translate([0,0,-rearShaftLength]) difference() {
    cylinder(d=shaftD, h=shaftLength+rearShaftLength+length);
    translate([-shaftD/2,shaftD/2-(shaftD-flatD),shaftLength+rearShaftLength+length-flatLength]) 
      cube([shaftD, (shaftD-flatD)*2, flatLength+1]); // shaft flat
    translate([0,0,shaftLength+rearShaftLength+length]) 
      cylindricalChamferOD(shaftD, shaftChamfer);// top chamfer
    rotate([180,0])
      cylindricalChamferOD(shaftD, shaftChamfer); // bottom chamfer
  }

  // bottom hole 6.8
  // top hole 8
  // 625 5x16x5 shielded
}

// Stepper motor with rounded corners.  Caps have consistent profile with body
module stepperMotorRounded(
    length, caseWidth, caseRadius,
    boltPatternD, boltD, boltDepth,
    topThickness, topProtrude, topProtrudeD, topHoleD,
    bottomThickness, bottomHoleD, 
    topRecessOD, topRecessID, topRecessDepth,
    shaftD, shaftLength, flatD, flatLength, rearShaftLength, shaftChamfer
  ) {

  color1 = [0.05,0.05,0.05];
  color2 = [0.15,0.15,0.15];
  alColor = [0.9,0.9,0.9];
  steelColor = [0.75,0.75,0.75];
  
  difference() {
    union() {
      color(color2) linear_extrude(bottomThickness, convexity=3) 
        difference() {
          roundedStepperProfile(caseWidth, caseRadius);
          circle(d=bottomHoleD);
        }
      color(color1) translate([0,0,bottomThickness]) linear_extrude(length-bottomThickness-topThickness) 
        roundedStepperProfile(caseWidth, caseRadius);
      color(color2) translate([0,0,length-topThickness]) linear_extrude(topThickness+0.1, convexity=3) 
        difference() {
          roundedStepperProfile(caseWidth, caseRadius);
          circle(d=topHoleD);
        }
    }
    
    
    // top face
    color(color2) translate([0,0,length-topRecessDepth]) difference() {
      cylinder(d=topRecessOD, h=topProtrude+1);
      rotate([180,0]) cylindricalChamferOD(topRecessOD, topRecessDepth);
      translate([0,0,-0.5]) cylinder(d=topProtrudeD-0.2,h=topProtrude*2);
    }
    
    // cut bare top face
    color(alColor) translate([0,0,length-0.02])
      linear_extrude(topProtrude+0.1, convexity=3) difference() {
        square(caseWidth+1,center=true);
        circle(d=(topProtrudeD+topRecessOD)/2, $fn=12);
      }
    
    // top bolt holes  
    color(alColor) translate([0,0,length-topThickness+1]) for (i=[0:3]) rotate(45+i*90) translate([boltPatternD/2,0]) cylinder(d=boltD, h=topThickness);

  }
  
  // top face protrusion
  translate([0,0,length]) difference() {
    color(alColor) translate([0,0,-topRecessDepth-0.1]) cylinder(d=topProtrudeD+0.1, h=topProtrude+topRecessDepth+0.1);
    color(color2) translate([0,0,topProtrude]) cube([topProtrudeD+1,topProtrudeD+1,1], center=true);
    color(color2) cylinder(d=topHoleD,h=(topProtrude+topRecessDepth)*2,center=true);
  }
  
  // motor shaft
  color(steelColor)
  translate([0,0,-rearShaftLength]) difference() {
    cylinder(d=shaftD, h=shaftLength+rearShaftLength+length);
    translate([-shaftD/2,shaftD/2-(shaftD-flatD),shaftLength+rearShaftLength+length-flatLength]) 
      cube([shaftD, (shaftD-flatD)*2, flatLength+1]); // shaft flat
    translate([0,0,shaftLength+rearShaftLength+length]) 
      cylindricalChamferOD(shaftD, shaftChamfer);// top chamfer
    rotate([180,0])
      cylindricalChamferOD(shaftD, shaftChamfer); // bottom chamfer
  }
  
}

module roundedStepperProfile(caseWidth, caseRadius) {
  if (caseRadius == caseWidth/2) {
    circle(r=caseRadius);
  } else if (caseRadius < caseWidth/2) {
    hull() for(i=[0:1],j=[0:1]) mirror([i,0]) mirror([0,j])
      translate([caseWidth/2-caseRadius,caseWidth/2-caseRadius]) circle(r=caseRadius);
  } else { // caseRadius > caseWidth/2
    intersection() {
      square(caseWidth, center=true);
      circle(r=caseRadius);
    }
  }
}

module cylindricalChamferOD(d, chamfer) {
  rotate_extrude() polygon([[d/2-chamfer,0],[d/2-chamfer,chamfer],[d/2+chamfer,chamfer],[d/2+chamfer,-chamfer],[d/2,-chamfer]]);
}

// generate a shape to be subtracted from an object to generate an innner chamfer
module cylindricalChamferID(d, chamfer) {
  rotate_extrude() polygon([[d/2+chamfer,0],[d/2+chamfer,chamfer],[d/2-chamfer,chamfer],[d/2-chamfer,-chamfer],[d/2,-chamfer]]);
}
