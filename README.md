# SelfBalancingRobot

# Global inspiration

## SelfBalancingRobot

[Part 1 video](https://www.youtube.com/watch?v=6WWqo-Yr8lA)
[Part 2 video](https://www.youtube.com/watch?v=VxpMWncBKZc)
[Part 3 video](https://www.youtube.com/watch?v=mG4OtAiY_wU)

[Blog Article](http://www.brokking.net/yabr_main.html)


## IMU tutorial video

[Part 1 video](https://www.youtube.com/watch?v=4BoIE8YQwM8)
[Part 2 video](https://www.youtube.com/watch?v=j-kE0AMEWy4)

[Blog Article](http://www.brokking.net/imu.html)

## Symax controller

[library](https://github.com/Suxsem/symaxrx)
[Remote controller hack](https://www.firstquadcopter.com/news/syma-x5c-range-hack)
# Target project

* Self balanced system
** System managed bu AtMega328P
* Syma remote controls system
** Syma reciever plugged on ESP32
* Backbone communication ESP32 / AtMega328p

## Symax receiver mixer system



# Hardware

* Teensy 2 ++
* MPU6050
* Steppers controller A4988
* Steppers Nema 17
* NRF24L01
* Syma quadcopter controller

## Step 1

### Building and testing effective IMU for the self balanced robot

* testing getting data with MPU library
* testing direct reading of MPU
* writing calibration script
* performance readings tests

## Step 2

### Testing timers for nema speed controll

* Playing with timers configurations and periods
* writing routine to control tick speed
* Converting relative speed to the right period
* defining periods min and max boudaries

## Step 3

### Implementing PID controller

* Testing external library for PID control
* Testing implementation of own PID class


## Step 4

### Control

* Create subproject 
* Create a real 4channels + 2 on / off servo receiver
* Test control with SYMA remote controller
* Test reimplemented control protocol
* https://www.youtube.com/watch?v=vWv1shiZPkg
* Official library found: https://github.com/Suxsem/symaxrx
* Test all compatible 2,4Ghz commands
* Check time for retriving command data

## Step 5

### Submodules

* Try to use AtMega328p for the low level controls
** Should be responsible reading IMU
** Control Motors
** Can communicate I2C with the parent module for commands and feed backs
* Optimising performances of continusly running routing
* Adding ESP32 for hisgh level decisions
* Working with sub controller for motion and ballancing



## 3D model

Support for Nema 17 and body
https://www.thingiverse.com/thing:2526

Configurable wheel
https://www.thingiverse.com/thing:21486

## Own 3D Models todo

* Electronic card holder fits in Support for Nema17
* Supports for Nema17 join component

## Missing omponents list

* [T connector for battery](https://www.amazon.fr/RUNCCI-Connecteurs-Puissance-Thermor%C3%A9tractable-Batterie/dp/B07VCPKWGN/ref=sr_1_23?crid=UD028131MM54&keywords=connecteur+en+t+pour+lipo&qid=1651059393&sprefix=connecteur+T+lipo%2Caps%2C49&sr=8-23)

