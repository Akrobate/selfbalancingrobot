
// nema cube size
// cube([56.5, 56.5, 56.5], center=true);


module block(center=true) {
    if (center) {
        translate([0, 0, -56.5 / 2])
            import("assets/Nema_Block.stl");
    } else {
        translate([56.5 / 2, 56.5 / 2, 0])
            import("assets/Nema_Block.stl");
    }    
}


module motorLevel() {
    translate([-56.5 / 2, 0])
        rotate([0, 180, 90])
            block(center = true);

    translate([56.5 / 2, 0])
        rotate([0, 180, 90])
            block(center = true);
}


module gelericLevel() {
    translate([-56.5 / 2, 0])
        rotate([90, 0, 90])
            block(center = true);

    translate([56.5 / 2, 0])
        rotate([90, 0, -90])
            block(center = true);
}


module bodyComponent() {
    translate([0, 0, 56.5 / 2])
        motorLevel();

    translate([0, 0, 56.5 / 2 + 56.5])
        gelericLevel();

    translate([0, 0, 56.5 / 2 + 56.5 * 2])
        gelericLevel();
}


// bodyComponent();


block(center = true);

cube([56.5, 56.5, 5], center=true);

nema_fix_distance = 31;

translate([- nema_fix_distance / 2, - nema_fix_distance / 2, 0])
    cylinder(d=3, h=150, center=true);

translate([- nema_fix_distance / 2,  nema_fix_distance / 2, 0])
    cylinder(d=3, h=150, center=true);

translate([ nema_fix_distance / 2,  nema_fix_distance / 2, 0])
    cylinder(d=3, h=150, center=true);

translate([ nema_fix_distance / 2, - nema_fix_distance / 2, 0])
    cylinder(d=3, h=150, center=true);


